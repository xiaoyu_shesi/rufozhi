<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends CI_Controller{
	
	public function view($page = 'home'){
		if(!file_exists('application/views/pages/about.php')){
			//页面不存在
			show_404();
		}
		
		$data['title'] = ucfirst($page); //将title的第一个字符大写
		
		$this->load->view('templates/header',$data);
		$this->load->view('pages/'.$page,$data);
		$this->load->view('templates/footer',$data);
	}
}


/* End of file pages.php */
/* Location: .\Test\application\controllers\pages.php */